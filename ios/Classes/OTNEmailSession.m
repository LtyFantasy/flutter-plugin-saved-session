//
//  OTNEmailSession.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNEmailSession.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNEmailSession

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [[super toDictionary] mutableCopy];
    NSDictionary *auth =  @{@"type": @"email",
                            @"email": self.email,
                            @"password": self.password,
                            };
    [result setObject:auth forKey:@"auth"];
    return result;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.password forKey:@"password"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.password = [aDecoder decodeObjectForKey:@"password"];
    }
    return self;
}
@end
