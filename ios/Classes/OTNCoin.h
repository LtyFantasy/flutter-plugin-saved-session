//
//  OTNCoin.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OTNCoin : NSObject
@property (nonatomic, assign) NSUInteger coinNumber;
@property (nonatomic, strong) NSString *promotionCode;
@property (nonatomic, assign) NSUInteger promotionReward;
@property (nonatomic, assign) NSUInteger promotionMaxUse;
- (NSDictionary *)toDictionary;
@end

NS_ASSUME_NONNULL_END
