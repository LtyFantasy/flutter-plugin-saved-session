//
//  OCWPSession.h
//  Runner
//
//  Created by elvin on 2019/6/17.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OTNOptionItem.h"
#import "OTNFilter.h"
#import "OTNCoin.h"
#import "OTNUser.h"


@interface OTNSession : NSObject <NSCoding>
@property (nonatomic, assign) BOOL keep;
@property (nonatomic, assign) long conversationUpdatedAt;


@property (nonatomic, copy) NSString *host;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *jwt;
@property (nonatomic, strong) NSString *tokenRongCloud;
@property (nonatomic, assign) NSTimeInterval expireInterval;
@property (nonatomic, strong, readonly) NSDate *expire;
@property (nonatomic, assign) BOOL supplement;
@property (nonatomic, assign) NSTimeInterval banned;
@property (nonatomic, strong) NSDate *registedDate;

@property (nonatomic, strong) OTNUser *user;
@property (nonatomic, strong) OTNCoin *coin;
@property (nonatomic, strong) OTNFilter *filter;
- (NSDictionary *)toDictionary;
@end
