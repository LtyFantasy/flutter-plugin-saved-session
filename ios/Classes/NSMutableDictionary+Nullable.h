//
//  NSMutableDictionary+Nullable.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableDictionary (Nullable)
+ (void)tn_dict:(nonnull NSMutableDictionary *)dict setObject:(nullable id)obj forKey:(nonnull id<NSCopying>)key;
- (void)tn_setNAObject:(nullable id)obj forKey:(nonnull id<NSCopying>)key;
- (void)tn_setNADate:(nullable NSDate *)date forKey:(nonnull id<NSCopying>)key;
@end

