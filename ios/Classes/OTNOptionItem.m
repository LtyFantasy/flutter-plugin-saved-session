//
//  TNOptionItem.m
//
//  Created by TeamN on 19-3-3.
//  Copyright (c) 2019年 TeamN. All rights reserved.
//

#import "OTNOptionItem.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNOptionItem

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result tn_setNAObject:self.key forKey:@"key"];
    [result tn_setNAObject:self.text forKey:@"text"];
    return result;
}

#pragma mark - NSCopying
- (id)copyWithZone:(NSZone *)zone{
    OTNOptionItem *item = [[[self class] allocWithZone:zone] init];
    item.text = [self.text copy];
    item.key = [self.key copy];
    return item;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.key forKey:@"key"];
    [aCoder encodeObject:self.text forKey:@"text"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.key = [aDecoder decodeObjectForKey:@"key"];
        self.text = [aDecoder decodeObjectForKey:@"text"];
    }
    return self;
}

@end
