//
//  OTNCoin.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNCoin.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNCoin

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject:@(self.coinNumber) forKey:@"balance"];
    if([self.promotionCode length] > 0) {
        NSMutableDictionary *promotion = [NSMutableDictionary dictionary];
        [promotion setObject:self.promotionCode forKey:@"code"];
        [promotion setObject:@(self.promotionReward) forKey:@"reward"];
        [promotion setObject:@(self.promotionMaxUse) forKey:@"maxUse"];
        
        [result setObject:promotion forKey:@"promotion"];
    }
    
    return result;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:@(self.coinNumber) forKey:@"coin_number"];
    [aCoder encodeObject:self.promotionCode forKey:@"promotion_code"];
    [aCoder encodeObject:@(self.promotionReward) forKey:@"promotion_reward"];
    [aCoder encodeObject:@(self.promotionMaxUse) forKey:@"promotion_max_use"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.coinNumber = [[aDecoder decodeObjectForKey:@"coin_number"] floatValue];
        self.promotionCode = [aDecoder decodeObjectForKey:@"promotion_code"];
        self.promotionReward = [[aDecoder decodeObjectForKey:@"promotion_reward"] integerValue];
        self.promotionMaxUse = [[aDecoder decodeObjectForKey:@"promotion_max_use"] integerValue];
        
    }
    return self;
}

@end
