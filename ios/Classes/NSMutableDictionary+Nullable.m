//
//  NSMutableDictionary+Nullable.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "NSMutableDictionary+Nullable.h"

@implementation NSMutableDictionary (Nullable)

+ (void)tn_dict:(nonnull NSMutableDictionary *)dict setObject:(nullable id)obj forKey:(nonnull id<NSCopying>)key{
    if(obj != nil){
        [dict setObject:obj forKey:key];
    }
}
- (void)tn_setNAObject:(nullable id)obj forKey:(nonnull id<NSCopying>)key{
    if(obj != nil){
        [self setObject:obj forKey:key];
    }
}
- (void)tn_setNADate:(nullable NSDate *)date forKey:(nonnull id<NSCopying>)key{
    if(date != nil){
        [self setObject:@([date timeIntervalSince1970]) forKey:key];
    }
}
@end
