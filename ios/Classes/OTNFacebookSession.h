//
//  OTNFacebookSession.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNSession.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTNFacebookSession : OTNSession

@end

NS_ASSUME_NONNULL_END
