//
//  OCWPSession.m
//  Runner
//
//  Created by elvin on 2019/6/17.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNSession.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNSession

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result setObject:@(self.keep == YES) forKey:@"keep"];
    [result setObject:@(self.conversationUpdatedAt) forKey:@"conversationUpdatedAt"];
    [result tn_setNAObject:self.host forKey:@"host"];
    [result tn_setNAObject:self.jwt forKey:@"jwt"];
    [result tn_setNAObject:self.token forKey:@"token"];
    [result tn_setNAObject:self.tokenRongCloud forKey:@"tokenRongCloud"];
    [result tn_setNAObject:@(self.expireInterval) forKey:@"expireInterval"];
    [result tn_setNAObject:@(self.supplement) forKey:@"supplement"];
    [result tn_setNADate:self.registedDate forKey:@"registeredDate"];
    [result tn_setNAObject:@(self.banned) forKey:@"banned"];
    
    [result tn_setNAObject:[self.user toDictionary] forKey:@"user"];
    [result tn_setNAObject:[self.coin toDictionary] forKey:@"coin"];
    [result tn_setNAObject:[self.filter toDictionary] forKey:@"filter"];
    
    return result;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.host forKey:@"host"];
    [aCoder encodeObject:self.jwt forKey:@"jwt"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.tokenRongCloud forKey:@"tokenRongCloud"];
    [aCoder encodeObject:@(self.expireInterval) forKey:@"expireInterval"];
    [aCoder encodeObject:@(self.supplement) forKey:@"supplement"];
    [aCoder encodeObject:self.registedDate forKey:@"registedDate"];
    [aCoder encodeObject:@(self.banned) forKey:@"banned"];
    
    [aCoder encodeObject:self.user forKey:@"user"];
    [aCoder encodeObject:self.coin forKey:@"coin"];
    [aCoder encodeObject:self.filter forKey:@"filter"];
    
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.host = [aDecoder decodeObjectForKey:@"host"];
        self.jwt = [aDecoder decodeObjectForKey:@"jwt"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.tokenRongCloud = [aDecoder decodeObjectForKey:@"tokenRongCloud"];
        self.expireInterval = [[aDecoder decodeObjectForKey:@"expireInterval"] doubleValue];
        self.supplement = [[aDecoder decodeObjectForKey:@"supplement"] doubleValue];
        self.registedDate = [aDecoder decodeObjectForKey:@"registedDate"];
        self.banned = [[aDecoder decodeObjectForKey:@"banned"] doubleValue];
        
        self.user = [aDecoder decodeObjectForKey:@"user"];
        self.coin = [aDecoder decodeObjectForKey:@"coin"];
        self.filter = [aDecoder decodeObjectForKey:@"filter"];
    }
    return self;
}
@end
