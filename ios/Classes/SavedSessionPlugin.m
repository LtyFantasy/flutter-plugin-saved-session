#import "SavedSessionPlugin.h"
#import "TNKeyChainHelper.h"
#import "OTNSession.h"
#import "OTNEmailSession.h"
#import "OTNFacebookSession.h"

#define kWPDefaultKeySessionKeep @"com.teamn.wooplus.session.keep"
#define kWPDefaultKeySession @"com.teamn.wooplus.session"

@implementation SavedSessionPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"saved_session_plugin"
            binaryMessenger:[registrar messenger]];
  SavedSessionPlugin* instance = [[SavedSessionPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else if([@"getSavedSession" isEqualToString:call.method]){
       result([SavedSessionPlugin savedSession]);
   } else if([@"clearSavedSession" isEqualToString:call.method]){
       [SavedSessionPlugin clearSavedSession];
        result(0);
    } else {
      result(FlutterMethodNotImplemented);
  }
}



+ (NSDictionary *)savedSession{
    NSData *data = [TNKeyChainHelper loadService:kWPDefaultKeySession];
    if(data == nil){
        NSLog(@">>>> NO STORED SESSION");
        return nil;
    }
    
    NSDictionary *result = nil;
    OTNSession *session = nil;
    @try {
        [NSKeyedUnarchiver setClass:[OTNEmailSession class] forClassName:@"TNResEmailSession"];
        [NSKeyedUnarchiver setClass:[OTNFacebookSession class] forClassName:@"TNResFacebookSession"];
        [NSKeyedUnarchiver setClass:[OTNSession class] forClassName:@"TNResSession"];
        [NSKeyedUnarchiver setClass:[OTNUser class] forClassName:@"TNResStatusUser"];
        [NSKeyedUnarchiver setClass:[OTNOptionItem class] forClassName:@"TNDictionaryItem"];
        [NSKeyedUnarchiver setClass:[OTNOptionItem class] forClassName:@"TNOptionItem"];
        [NSKeyedUnarchiver setClass:[OTNFilter class] forClassName:@"TNFilter"];
        [NSKeyedUnarchiver setClass:[OTNCoin class] forClassName:@"TNCoins"];
        
        session = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        session.keep = [[NSUserDefaults standardUserDefaults] boolForKey:kWPDefaultKeySessionKeep];
        
        result = [session toDictionary];
    }
    @catch (NSException *exception) {
        NSLog(@">>>> Unarchiver Exception=%@", exception);
    }
    @finally {
    }
    
    return session == nil ? @{} : result;
}

+ (void)clearSavedSession{
    [TNKeyChainHelper deleteService:kWPDefaultKeySession];
}

@end
