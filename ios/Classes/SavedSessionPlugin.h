#import <Flutter/Flutter.h>

@interface SavedSessionPlugin : NSObject<FlutterPlugin>
+ (NSDictionary *)savedSession;
@end
