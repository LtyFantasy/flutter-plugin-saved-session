//
//  OTNUser.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNUser.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNUser

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result tn_setNAObject:self.userId forKey:@"userId"];
    [result tn_setNAObject:self.displayName forKey:@"displayName"];
    [result tn_setNAObject:@(self.gender) forKey:@"gender"];
    [result tn_setNAObject:@(self.isVIP) forKey:@"isVIP"];
    
    [result tn_setNAObject:@(self.age) forKey:@"age"];
    [result tn_setNAObject:self.interests forKey:@"interests"];
    [result tn_setNAObject:self.address forKey:@"address"];
    
    
    [result tn_setNAObject:@(self.status) forKey:@"status"];
    [result tn_setNADate:self.vipExpire forKey:@"vipExpire"];
    [result tn_setNADate:self.createdAt forKey:@"createdAt"];
    if([self.longitude length] > 0)
        [result tn_setNAObject:@([self.longitude doubleValue]) forKey:@"longitude"];
    if([self.latitude length] > 0)
        [result tn_setNAObject:@([self.latitude doubleValue]) forKey:@"latitude"];
    [result tn_setNAObject:self.countryIso forKey:@"countryIso"];
    
    return result;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.displayName forKey:@"displayName"];
    [aCoder encodeObject:@(self.gender) forKey:@"gender"];
    [aCoder encodeObject:@(self.isVIP) forKey:@"isVIP"];
    
    [aCoder encodeObject:@(self.age) forKey:@"age"];
    [aCoder encodeObject:self.interests forKey:@"interests"];
    [aCoder encodeObject:self.address forKey:@"address"];
    
    [aCoder encodeObject:@(self.status) forKey:@"status"];
    [aCoder encodeObject:self.vipExpire forKey:@"vipExpire"];
    [aCoder encodeObject:self.createdAt forKey:@"createdAt"];
    [aCoder encodeObject:self.longitude forKey:@"longitude"];
    [aCoder encodeObject:self.latitude forKey:@"latitude"];
    [aCoder encodeObject:self.countryIso forKey:@"country_iso"];
}
- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.userId = [aDecoder decodeObjectForKey:@"userId"];
        self.displayName = [aDecoder decodeObjectForKey:@"displayName"];
        self.gender = [[aDecoder decodeObjectForKey:@"gender"] integerValue];
        self.isVIP = [[aDecoder decodeObjectForKey:@"isVIP"] boolValue];
        
        self.age = [[aDecoder decodeObjectForKey:@"age"] integerValue];
        self.interests = [aDecoder decodeObjectForKey:@"interests"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        
        self.status = [[aDecoder decodeObjectForKey:@"status"] integerValue];
        self.vipExpire = [aDecoder decodeObjectForKey:@"vipExpire"];
        self.createdAt = [aDecoder decodeObjectForKey:@"createdAt"];
        self.longitude = [aDecoder decodeObjectForKey:@"longitude"];
        self.latitude = [aDecoder decodeObjectForKey:@"latitude"];
        self.countryIso = [aDecoder decodeObjectForKey:@"country_iso"];
    }
    return self;
}

@end
