//
//  OTNUser.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, Gender) {
    GenderMale      = 1 << 0,
    GenderFemale    = 1 << 1,
    GenderBoth      = (GenderMale | GenderFemale),
};

@interface OTNUser : NSObject <NSCoding>
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, assign) Gender gender;
@property (nonatomic, assign) BOOL isVIP;

@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, strong) NSArray<NSNumber *> *interests;
@property (nonatomic, strong) NSString *address;

@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSDate *vipExpire;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, assign) BOOL isMainPhotoDeleted;

@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *countryIso;
- (NSDictionary *)toDictionary;
@end

NS_ASSUME_NONNULL_END
