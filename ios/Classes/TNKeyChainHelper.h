//
//  TNKeyChainHelper.h
//
//  Created by TeamN on 17-10-31.
//  Copyright (c) 2017年 TeamN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface TNKeyChainHelper : NSObject

+ (OSStatus)saveService:(NSString *)service data:(id)data;
+ (id)loadService:(NSString *)service;
+ (void)deleteService:(NSString *)service;

@end
