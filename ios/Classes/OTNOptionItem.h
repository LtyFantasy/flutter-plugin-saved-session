//
//  TNOptionItem.h
//
//  Created by TeamN on 19-3-3.
//  Copyright (c) 2019年 TeamN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OTNOptionItem : NSObject <NSCoding>
@property (nonatomic, strong)  NSString *key;
@property (nonatomic, strong)  NSString *text;
- (NSDictionary *)toDictionary;
@end
