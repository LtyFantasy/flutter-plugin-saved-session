//
//  OTNFacebookSession.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNFacebookSession.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNFacebookSession
- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [[super toDictionary] mutableCopy];
    NSDictionary *auth = @{@"type": @"facebook"};
    [result setObject:auth forKey:@"auth"];
    return result;
}
@end
