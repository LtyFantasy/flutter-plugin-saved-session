//
//  OCWPFilter.m
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNFilter.h"
#import "NSMutableDictionary+Nullable.h"

@implementation OTNFilter

- (NSDictionary *)toDictionary{
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    [result tn_setNAObject:[self.filterGender toDictionary] forKey:@"gender"];
    [result tn_setNAObject:self.maxAge forKey:@"maxAge"];
    [result tn_setNAObject:self.minAge forKey:@"minAge"];
    return result;
}

#pragma mark - NSCoding
- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.filterGender forKey:@"fitlerGender"];
    [aCoder encodeObject:self.maxAge forKey:@"maxAge"];
    [aCoder encodeObject:self.minAge forKey:@"minAge"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if(self){
        self.filterGender = [aDecoder decodeObjectForKey:@"fitlerGender"];
        self.maxAge = [aDecoder decodeObjectForKey:@"maxAge"];
        self.minAge = [aDecoder decodeObjectForKey:@"minAge"];
    }
    return self;
}
@end
