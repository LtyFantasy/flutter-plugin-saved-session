//
//  OTNEmailSession.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import "OTNSession.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTNEmailSession : OTNSession
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@end

NS_ASSUME_NONNULL_END
