//
//  TNKeyChainHelper.m
//
//  Created by TeamN on 17-10-31.
//  Copyright (c) 2017年 TeamN. All rights reserved.
//

#import "TNKeyChainHelper.h"
#import <Security/Security.h>

@implementation TNKeyChainHelper

+ (NSMutableDictionary *)getKeychainDictionary:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge id)kSecClassGenericPassword,(__bridge id)kSecClass,
            (__bridge id)kSecAttrAccessibleAfterFirstUnlock,(__bridge id)kSecAttrAccessible,
            service, (__bridge id)kSecAttrService,
            service, (__bridge id)kSecAttrAccount,
            nil];
}

+ (OSStatus)saveService:(NSString *)service data:(id)data {
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    SecItemDelete((__bridge CFDictionaryRef)dictionary);
    [dictionary setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge id)kSecValueData];
    return SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
}

+ (id)loadService:(NSString *)service {
    id ret = nil;
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    [dictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [dictionary setObject:(__bridge id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    CFDataRef data = NULL;

    OSStatus code = SecItemCopyMatching((__bridge CFDictionaryRef)dictionary, (CFTypeRef *)&data);
    if (code == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge NSData *)data];
        } @catch (NSException *e) {
            NSLog(@"TNKeyChainHelper loadService %@ failed: %@", service, e);
        } @finally {
        }
    }
    if (data)
        CFRelease(data);
    return ret;
}

+ (void)deleteService:(NSString *)service {
    NSMutableDictionary *dictionary = [self getKeychainDictionary:service];
    SecItemDelete((__bridge CFDictionaryRef)dictionary);
}

@end
