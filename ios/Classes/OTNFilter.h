//
//  OCWPFilter.h
//  Runner
//
//  Created by elvin on 2019/6/18.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OTNOptionItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface OTNFilter : NSObject <NSCoding>
@property (nonatomic, copy) OTNOptionItem *filterGender;
@property (nonatomic, strong) NSNumber *maxAge;
@property (nonatomic, strong) NSNumber *minAge;
- (NSDictionary *)toDictionary;
@end

NS_ASSUME_NONNULL_END
