import 'dart:async';

import 'package:flutter/services.dart';
import 'src/saved_session.dart';

export 'src/saved_auth.dart';
export 'src/saved_session.dart';
export 'src/saved_user.dart';
export 'src/saved_coin.dart';
export 'src/saved_filter.dart';

class SavedSessionPlugin {
  static const MethodChannel _channel =
      const MethodChannel('saved_session_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }


  static Future<SavedSession> get savedSession async {
    final map = await _channel.invokeMethod('getSavedSession');
    return map != null ? SavedSession.fromMap(map) : null;
  }


  static Future<void> clearSavedSession() async {
    await _channel.invokeMethod('clearSavedSession');
  }
}
