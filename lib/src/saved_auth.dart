class SavedAuth {
  static const authTypeFacebook = 'facebook';
  static const authTypeEmail = 'email';

  String type;

  SavedAuth(this.type);

  factory SavedAuth.fromMap(Map map){
    if(map == null) return null;
    String type = map['type'];
    if (type == authTypeFacebook) {
      return SavedFacebookAuth();
    }else if(type == authTypeEmail){
      return SavedEmailAuth(map['email'], map['password']);
    }
    return null;
  }
}

class SavedEmailAuth extends SavedAuth {
  String email;
  String password;

  SavedEmailAuth(this.email, this.password) : super(SavedAuth.authTypeEmail);
}

class SavedFacebookAuth extends SavedAuth {
  SavedFacebookAuth() : super(SavedAuth.authTypeFacebook);
}