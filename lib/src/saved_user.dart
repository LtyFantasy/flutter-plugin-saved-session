import 'dart:io';

class SavedUser {
  String userId;
  String displayName;
  int gender;
  bool isVIP;
  int age;

  List<int> interests;
  DateTime expirationOfVip;
  DateTime createdAt;
  int status;

  String address;
  double longitude;
  double latitude;
  String isoCountryCode;

  SavedUser.fromMap(Map map) {
    this.userId = map['userId'];
    this.displayName = map['displayName'];
    this.gender = map['gender'];
    this.age = map['age'];
    this.isVIP = map['isVIP'];
    if (map['vipExpire'] != null) {
      final expire = map['vipExpire'];
      final timestamp = (expire is double ? expire.toInt() : expire) * 1000;
      this.expirationOfVip = DateTime.fromMillisecondsSinceEpoch(timestamp);
    }
    List<dynamic> interests = map['interests'];
    this.interests = interests.map<int>((i) {
      if (Platform.isIOS) {
        return i as int;
      } else {
        return int.parse(i);
      }
    }).toList();
    final createdAt = map['createdAt'];
    if (createdAt != null)
      this.createdAt = DateTime.fromMillisecondsSinceEpoch(
          (createdAt * 1000).toInt(),
          isUtc: true);
    this.status = map['status'];

    this.address = map['address'];
    if (map['longitude'] != null)
      this.longitude = double.parse(map['longitude'].toString());
    if (map['latitude'] != null)
      this.latitude = double.parse(map['latitude'].toString());
    this.isoCountryCode = map['countryIso'];
  }
}
