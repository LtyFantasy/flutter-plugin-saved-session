//NSMutableDictionary *result = [NSMutableDictionary dictionary];
//[result setObject:@(self.coinNumber) forKey:@"balance"];
//if(self.promotionCode) {
//NSMutableDictionary *promotion = [NSMutableDictionary dictionary];
//[promotion setObject:self.promotionCode forKey:@"code"];
//[promotion setObject:@(self.promotionReward) forKey:@"reward"];
//[promotion setObject:@(self.promotionMaxUse) forKey:@"maxUse"];
//
//[result setObject:promotion forKey:@"promotion"];
//}



class SavedCoin{
  int balance;
  SavedPromotion promotion;

  SavedCoin.fromMap(Map map){
    balance = 0;
    if(map['balance'] != null) {
      final b = map['balance'];
      if(b is int) balance = b;
      if(b is double) balance = b.toInt();
      if(b is String) {
        try{
          balance = int.parse(b);
        }catch(e){}
      }
    }
    if(map['promotion'] != null){
      this.promotion = SavedPromotion.fromMap(map['promotion']);
    }
  }
}


class SavedPromotion{
  String code;
  int reward;
  int maxUse;

  SavedPromotion.fromMap(Map map){
    this.code = map['code'];
    this.reward = map['reward'];
    this.maxUse = map['maxUse'];
  }
}