
class SavedOptionItem{
  String key;
  String text;

  SavedOptionItem.fromMap(Map map){
    this.key = map['key'].toString();
    this.text = map['text'];
  }
}