import 'saved_option_item.dart';

class SavedFilter {
  SavedOptionItem gender;
  int minAge;
  int maxAge;

  SavedFilter.fromMap(Map map) {
    this.gender =
        map['gender'] != null ? SavedOptionItem.fromMap(map['gender']) : null;
    this.minAge = map['minAge'];
    this.maxAge = map['maxAge'];
  }
}
