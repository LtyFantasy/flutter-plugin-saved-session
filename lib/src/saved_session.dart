import 'saved_auth.dart';
import 'saved_user.dart';
import 'saved_filter.dart';
import 'saved_coin.dart';

//@property (nonatomic, copy)   NSString *host;
//@property (nonatomic, strong) NSString *token;
//@property (nonatomic, strong) NSString *jwt;
//@property (nonatomic, strong) NSString *tokenRongCloud;
//@property (nonatomic, assign) NSTimeInterval expireInterval;
//@property (nonatomic, strong, readonly) NSDate *expire;

//@property (nonatomic, assign) BOOL supplement;
//@property (nonatomic, assign) NSTimeInterval banned;
//@property (nonatomic, strong) NSDate *registedDate;
//
//@property (nonatomic, strong) OTNUser *user;
//@property (nonatomic, strong) OTNCoin *coin;
//@property (nonatomic, strong) OTNFilter *filter;
class SavedSession {
  bool keep;
  SavedAuth auth;
  String host;
  String token;
  String jwt;
  String tokenRongCloud;
  double banned;
  DateTime registeredDate;

  SavedUser user;
  SavedCoin coin;
  SavedFilter filter;

  SavedSession.fromMap(Map map) {
    this.keep = map['keep'] == 1;
    this.auth = SavedAuth.fromMap(map['auth']);

    this.host = map['host'];
    this.token = map['token'];
    this.jwt = map['jwt'];
    this.tokenRongCloud = map['tokenRongCloud'];
    this.banned = double.parse(map['banned'].toString());
    final registeredDate = map['registeredDate'];
    if (registeredDate != null)
      this.registeredDate = DateTime.fromMillisecondsSinceEpoch(
          (registeredDate * 1000).toInt(),
          isUtc: true);

    this.user = SavedUser.fromMap(map['user']);
    this.coin = SavedCoin.fromMap(map['coin']);
    this.filter = SavedFilter.fromMap(map['filter']);
  }
}
