#include "AppDelegate.h"
#include "GeneratedPluginRegistrant.h"
#import <saved_session_plugin/SavedSessionPlugin.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [GeneratedPluginRegistrant registerWithRegistry:self];
    // Override point for customization after application launch.
    NSDictionary *session = [SavedSessionPlugin savedSession];
    NSLog(@"%@", session);
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

@end
