package com.mason.wooplus.bean;

import java.io.Serializable;

public class SessionBean implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4534893547402183816L;
    private UserSessionBean session;
    private static volatile SessionBean sessionBean;
    private boolean isLogOut = false;

    public static SessionBean getSessionBean() {

        return sessionBean;
    }


    
    public UserSessionBean getSession() {
        return session;
    }
    public void setSession(UserSessionBean session) {
        this.session = session;
    }

}
