package com.mason.wooplus.bean;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CoinBean implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6447080534663740607L;
    private int coin_number;
    private String promotion_code;
    private int promotion_max_use;
    private int promotion_reward;
    
    public int getCoin_number() {
        return coin_number;
    }
    public void setCoin_number(int coin_number) {
        int temp_coin_number = this.coin_number;
        this.coin_number = coin_number;
    }
    public String getPromotion_code() {
        return promotion_code;
    }
    public void setPromotion_code(String promotion_code) {
        this.promotion_code = promotion_code;
    }
    public int getPromotion_max_use() {
        return promotion_max_use;
    }
    public void setPromotion_max_use(int promotion_max_use) {
        this.promotion_max_use = promotion_max_use;
    }
    public int getPromotion_reward() {
        return promotion_reward;
    }
    public void setPromotion_reward(int promotion_reward) {
        this.promotion_reward = promotion_reward;
    }

}
