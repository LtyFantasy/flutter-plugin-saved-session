package com.mason.wooplus.bean;


import java.io.Serializable;
import java.util.List;

public class UserBean implements Serializable{

    private static final long serialVersionUID = -1884539770584548384L;
    private String address;
    private String body_size;
    private String body_type;
    private String display_name;
    private String email;
    private String photo_url;
    private String user_id;
    private List<String> interests;
    private int status;
    private int gender; //1 male , 2 female
    private int is_vip; //1 vip
    private long vip_expire;
    private String aboutme;
    private long created_at;
    private double longitude;
    private double latitude;
    private String country_iso;

    
    private static UserBean userBean;
    /**
     * mainphoto_object : {"pid":"","url":"","width":"","height":"","face_number":1,"status":0}
     */

    private MainphotoObjectBean mainphoto_object;

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getBody_size() {
        return body_size;
    }
    public void setBody_size(String body_size) {
        this.body_size = body_size;
    }
    public String getBody_type() {
        return body_type;
    }
    public void setBody_type(String body_type) {
        this.body_type = body_type;
    }
    public String getDisplay_name() {
        return display_name;
    }
    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhoto_url() {
        return photo_url;
    }
    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }
    public String getUser_id() {
        return user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public List<String> getInterests() {
        return interests;
    }
    public void setInterests(List<String> interests) {
        this.interests = interests;
    }
    public int getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getCountry_iso() {
        return country_iso;
    }

    public void setCountry_iso(String country_iso) {
        this.country_iso = country_iso;
    }

    public static UserBean getUserBean() {

        return userBean;
    }
    public static void setUserBean(UserBean userBean) {
        UserBean.userBean = userBean;
    }
    public int getIs_vip() {
        return is_vip;
    }
    public void setIs_vip(int is_vip) {
        this.is_vip = is_vip;
    }
    public boolean isVIP() {
        if(is_vip == 1){
            return true;
        }else{
            return false;
        }
    }
    public long getVip_expire() {
        return vip_expire;
    }
    public void setVip_expire(long vip_expire) {
        this.vip_expire = vip_expire;
    }

    public String getAboutme() {
        return aboutme;
    }

    public void setAboutme(String aboutme) {
        this.aboutme = aboutme;
    }

    public MainphotoObjectBean getMainphoto_object() {
        return mainphoto_object;
    }

    public void setMainphoto_object(MainphotoObjectBean mainphoto_object) {
        this.mainphoto_object = mainphoto_object;
    }

    public static class MainphotoObjectBean implements Serializable{
        /**
         * pid :
         * url :
         * width :
         * height :
         * face_number : 1
         * status : 0
         */

        private String pid;
        private String url;
        private String width;
        private String height;
        private int face_number;
        private int statusX;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

        public int getFace_number() {
            return face_number;
        }

        public void setFace_number(int face_number) {
            this.face_number = face_number;
        }

        public int getStatusX() {
            return statusX;
        }

        public void setStatusX(int statusX) {
            this.statusX = statusX;
        }
    }
}
