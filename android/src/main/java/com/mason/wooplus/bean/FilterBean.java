package com.mason.wooplus.bean;


import java.io.Serializable;

public class FilterBean implements Serializable, Cloneable {
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -6044186112840814824L;
    private int match_gender;
    private int max_age;
    private int min_age;
    private int type; //0 anywhere 1 region
    private String region_country;
    private String region_state;
    private boolean last_online;
    private String ethnicity;
    private String distance = "0";//千米
    private int location_type;//0=anywhere, 1=region, 2=distance

    private int wooplus_select_type; //0:noselect 1:hot 2:active 3:ethnicity


    private boolean cardsFilterChange = false;//卡片选项改变

    public boolean isCardsFilterChange() {
        return cardsFilterChange;
    }

    public void setCardsFilterChange(boolean cardsFilterChange) {
        this.cardsFilterChange = cardsFilterChange;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public int getWooplus_select_type() {
        return wooplus_select_type;
    }

    public void setWooplus_select_type(int wooplus_select_type) {
        this.wooplus_select_type = wooplus_select_type;
    }

    public int getMatch_gender() {
        return match_gender;
    }

    public void setMatch_gender(int match_gender) {
        this.match_gender = match_gender;
    }

    public int getMax_age() {
        return max_age;
    }

    public void setMax_age(int max_age) {
        this.max_age = max_age;
    }

    public int getMin_age() {
        return min_age;
    }

    public void setMin_age(int min_age) {
        this.min_age = min_age;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRegion_country() {
        return region_country;
    }

    public void setRegion_country(String region_country) {
        this.region_country = region_country;
    }

    public String getRegion_state() {
        return region_state;
    }

    public void setRegion_state(String region_state) {
        this.region_state = region_state;
    }

    public boolean isLast_online() {
        return last_online;
    }

    public void setLast_online(boolean last_online) {
        this.last_online = last_online;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public int getLocation_type() {
        return location_type;
    }

    public void setLocation_type(int location_type) {
        this.location_type = location_type;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        FilterBean object = null;
        try {
            object = (FilterBean) super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }



}
