package com.mason.wooplus.bean;

import java.io.Serializable;


public class UserSessionBean implements Serializable{

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2443868779699109605L;
    
    private int notification;
    private String token;
    private String expire;
    private String rong;
    private String supplement;
    private CoinBean coin;
    private ConfigBean config;
    private FilterBean filter;
    private UserBean user;
    private String jwt;

    public int getNotification() {
        return notification;
    }
    public void setNotification(int notification) {
        this.notification = notification;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getExpire() {
        return expire;
    }
    public void setExpire(String expire) {
        this.expire = expire;
    }
    public String getRong() {
        return rong;
    }
    public void setRong(String rong) {
        this.rong = rong;
    }
    public String getSupplement() {
        return supplement;
    }
    public void setSupplement(String supplement) {
        this.supplement = supplement;
    }
    public CoinBean getCoin() {
        return coin;
    }
    public void setCoin(CoinBean coin) {
        this.coin = coin;
    }
    public ConfigBean getConfig() {
        return config;
    }
    public void setConfig(ConfigBean config) {
        this.config = config;
    }
    public FilterBean getFilter() {
        return filter;
    }
    public void setFilter(FilterBean filter) {
        if(filter == null) return;
        this.filter = filter;
    }
    public UserBean getUser() {
        return user;
    }
    public void setUser(UserBean user) {
        this.user = user;
    }
    public String getJwt() {
        return jwt;
    }
    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
    public boolean isSupplement(){
        return "1".equals(supplement);
    }
    public void setSupplemented(){
        this.supplement = "0";
    }
    
    public boolean isRong(){
       return true;
    }

}
