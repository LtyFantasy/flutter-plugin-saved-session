package com.mason.wooplus.bean;

import java.io.Serializable;

public class SignInTypeBean implements Serializable{

    private static final long serialVersionUID = -595088377425200525L;
    public static final int type_email = 1;
    public static final int type_facebook = 2;
    private int type = -1;
    private String email;
    private String password;
    
    public SignInTypeBean(int type, String email, String password){
        this.type = type;
        this.email = email;
        this.password = password;
    }
    
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
