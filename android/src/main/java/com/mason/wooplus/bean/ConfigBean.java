package com.mason.wooplus.bean;

import java.io.Serializable;

public class ConfigBean implements Serializable{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -5912381953929516252L;
    private String openfire;

    public String getOpenfire() {
        return openfire;
    }

    public void setOpenfire(String openfire) {
        this.openfire = openfire;
    }


}
