package com.teamn.wooplus.flutter.saved_session_plugin;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectToSerializeUtil {

    private static final char[] hexArray = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };
    private static final byte[] high = new byte[16];
    private static final byte[] low = new byte[16];
    static {
        byte b;

        for (int i = 0; i < low.length; i++) {
            b      = (byte) i;
            low[i] = (byte) (b & 0x0f);
        }

        for (int i = 0; i < high.length; i++) {
            b       = (byte) i;
            high[i] = (byte) ((b << 4) & 0xf0);
        }
    }
    
	public static Object getObject(String param){
		if(param == null) return null;
        try {
        	ByteArrayInputStream mem_in = new ByteArrayInputStream(ObjectToSerializeUtil.hexStringToBytes(param));
        	ObjectInputStream in = new ObjectInputStream(mem_in);
        	Object object = in.readObject();  
        	in.close();  
        	mem_in.close();  
        	return object;  
        }catch (Exception e) {
        	e.printStackTrace();
        }  
		return null;
	}
    
	public static byte[] getBytes(Object obj){
		try {
        	ByteArrayOutputStream mem_out = new ByteArrayOutputStream();
        	ObjectOutputStream out = new ObjectOutputStream(mem_out);
            out.writeObject(obj);
            byte[] bytes = mem_out.toByteArray();
            out.close();
            mem_out.close();
            return bytes;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
    public static String bytesToHexString(byte[] inByteArray) {
        return bytesToHexString(inByteArray, 0, inByteArray.length);
    }
	
    public static String bytesToHexString(byte[] inByteArray, int offset, int len) {
        if (inByteArray == null) {
            return null;
        }
        int position;
        StringBuffer returnBuffer = new StringBuffer();

        for (position = offset; position < len; position++) {
            returnBuffer.append(hexArray[((inByteArray[position] >> 4) & 0x0f)]);
            returnBuffer.append(hexArray[(inByteArray[position] & 0x0f)]);
        }
        String re=returnBuffer.toString();
        returnBuffer=null;
        return re;
    }
	
    public static byte[] hexStringToBytes(String str) {
        byte   b;
        byte   b2;
        int    len    = str.length();
        byte[] retval = new byte[len / 2];
        int    j      = 0;

        for (int i = 0; i < len; i += 2) {
            b           = high[getIndex(str.charAt(i))];
            b2          = low[getIndex(str.charAt(i + 1))];
            retval[j++] = (byte) (b | b2);
        }

        return retval;
    }
    
    private static int getIndex(char c) {
        if (('0' <= c) && (c <= '9')) {
            return ((byte) c - (byte) '0');
        } else if (('a' <= c) && (c <= 'f')) {
            return ((byte) c - (byte) 'a' + 10);
        } else if (('A' <= c) && (c <= 'F')) {
            return ((byte) c - (byte) 'A' + 10);
        } else {
            return -1;
        }
    }
    
}
