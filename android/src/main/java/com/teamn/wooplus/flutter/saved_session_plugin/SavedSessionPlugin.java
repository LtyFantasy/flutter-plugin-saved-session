package com.teamn.wooplus.flutter.saved_session_plugin;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.mason.wooplus.bean.CoinBean;
import com.mason.wooplus.bean.FilterBean;
import com.mason.wooplus.bean.SessionBean;
import com.mason.wooplus.bean.SignInTypeBean;
import com.mason.wooplus.bean.UserBean;
import com.mason.wooplus.bean.UserSessionBean;

import java.util.HashMap;
import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * SavedSessionPlugin
 */
public class SavedSessionPlugin implements MethodCallHandler {
    private static final String TAG = "SavedSessionPlugin";
    private static Context context;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "saved_session_plugin");
        channel.setMethodCallHandler(new SavedSessionPlugin());
        context = registrar.context();
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        if (call.method.equals("getPlatformVersion")) {
            result.success("Android " + android.os.Build.VERSION.RELEASE);
        } else if (call.method.equals("getSavedSession")) {
            SessionBean sessionBean = fetchSession(context);
            SignInTypeBean signInTypeBean = fetchSignType(context);

            if (sessionBean == null || signInTypeBean == null) {
                result.success(null);
            } else {
                UserSessionBean userSessionBean = sessionBean.getSession();
                if (userSessionBean == null) {
                    result.success(null);
                    return;
                }
                Map map = new HashMap();
                map.put("keep", 1);
                Map auth = new HashMap();
                if (signInTypeBean.getType() == SignInTypeBean.type_facebook) {
                    auth.put("type", "facebook");
                } else if (signInTypeBean.getType() == SignInTypeBean.type_email) {
                    auth.put("type", "email");
                    auth.put("email", signInTypeBean.getEmail());
                    auth.put("password", signInTypeBean.getPassword());
                }
                map.put("auth",auth);
                map.put("token", userSessionBean.getToken());
                map.put("jwt", userSessionBean.getJwt());
                map.put("tokenRongCloud", userSessionBean.getRong());
                map.put("banned", "0");

                map.put("user", userToMap(userSessionBean.getUser()));
                map.put("coin", coinToMap(userSessionBean.getCoin()));
                map.put("filter", filterToMap(userSessionBean.getFilter()));
                Log.i(TAG, "onMethodCall: "+map.toString());
                result.success(map);
            }
        } else if (call.method.equals("clearSavedSession")) {
            clear(context);
            result.success(null);
        } else {
            result.notImplemented();
        }
    }

    private Map userToMap(UserBean userBean) {
        Map map = new HashMap();
        map.put("userId", userBean.getUser_id());
        map.put("displayName", userBean.getDisplay_name());
        map.put("gender", userBean.getGender());
        map.put("isVIP", userBean.isVIP());
        map.put("vipExpire", userBean.getVip_expire());
        map.put("interests", userBean.getInterests());
        map.put("createdAt", userBean.getCreated_at());
        map.put("status", userBean.getStatus());
        map.put("countryIso", userBean.getCountry_iso());
        map.put("longitude", userBean.getLongitude());
        map.put("latitude", userBean.getLatitude());
        return map;
    }

    private Map coinToMap(CoinBean coinBean) {
        Map map = new HashMap();
        map.put("balance", coinBean.getCoin_number());
        Map promotion = new HashMap();
        promotion.put("code", coinBean.getPromotion_code());
        promotion.put("reward", coinBean.getPromotion_reward());
        promotion.put("maxUse", coinBean.getPromotion_max_use());

        map.put("promotion", promotion);
        return map;
    }

    private Map filterToMap(FilterBean filterBean) {
        Map map = new HashMap();
        map.put("minAge", filterBean.getMin_age());
        map.put("maxAge", filterBean.getMax_age());
        Map matchGender = new HashMap();
        matchGender.put("key", filterBean.getMatch_gender() + "");
        map.put("gender", matchGender);
        return map;
    }


    //SessionBean
    public static SessionBean fetchSession(Context context) {
        SharedPreferences settings = context.getSharedPreferences("user_session", Context.MODE_PRIVATE);
        String data = settings.getString("session", null);
        Object object = ObjectToSerializeUtil.getObject(data);
        if (object == null) return null;
        SessionBean sessionBean = (SessionBean) object;
        if (sessionBean.getSession() == null) return null;

        UserBean.setUserBean(sessionBean.getSession().getUser());
        return (SessionBean) object;
    }

    public static SignInTypeBean fetchSignType(Context context) {
        SharedPreferences settings = context.getSharedPreferences("loginType", Context.MODE_PRIVATE);
        String data = settings.getString("type", null);
        Object object = ObjectToSerializeUtil.getObject(data);
        if (object == null) return null;
        return (SignInTypeBean) object;
    }

    public static void clear(Context context) {
        SharedPreferences settings = context.getSharedPreferences("user_session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();
    }
}
