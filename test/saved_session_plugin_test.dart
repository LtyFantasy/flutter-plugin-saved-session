import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:saved_session_plugin/saved_session_plugin.dart';

void main() {
  const MethodChannel channel = MethodChannel('saved_session_plugin');

  const savedSessionJson = r'{"host":"api-sandbox.wooplus.com","auth":{"type":"email","email":"test@gmail.com","password":"test pwd"},"jwt":"test jwt","tokenRongCloud":"token of rong cloud","banned":1561400316,"registeredDate":1561430316,"user":{"userId":"test user id","displayName":"f520","gender":2,"isVIP":true,"age":35,"interests":[1,2,3,5],"address":"Chengdu Sichuan China","status":2,"vipExpire":0,"createdAt":1561430316,"longitude":130.2,"latitude":30.1,"countryIso":"CN"},"coin":{"balance":100},"filter":{"gender":{"key":3,"text":"Woman & Man"},"minAge":19,"maxAge":40}}';

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      if(methodCall.method == 'getPlatformVersion') {
        return '42';
      }else if(methodCall.method == 'getSavedSession'){
        return jsonDecode(savedSessionJson);
      }
      throw UnimplementedError;
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await SavedSessionPlugin.platformVersion, '42');
  });


  test('getSavedSession', () async {
    SavedSession session = await SavedSessionPlugin.savedSession;
    expect(session.jwt, 'test jwt');
    expect(session.auth is SavedEmailAuth, true);
    expect((session.auth as SavedEmailAuth).email, 'test@gmail.com');
    expect(session.registeredDate.millisecondsSinceEpoch, 1561430316 * 1000);

    expect(session.user.userId, 'test user id');
    expect(session.user.displayName, 'f520');
    expect(session.user.gender, 2);
    expect(session.user.isVIP, true);
    expect(session.user.age, 35);
    expect(session.user.interests, [1, 2, 3, 5]);
    expect(session.user.address, "Chengdu Sichuan China");
    expect(session.user.status, 2);
    expect(session.user.createdAt.millisecondsSinceEpoch, 1561430316 * 1000);
    expect(session.user.longitude, 130.2);
    expect(session.user.latitude, 30.1);
    expect(session.user.isoCountryCode, 'CN');

    expect(session.coin.balance, 100);

    expect(session.filter.gender.key, '3');
    expect(session.filter.minAge, 19);
    expect(session.filter.maxAge, 40);

  });
}
